# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Dummy::Application.config.secret_key_base = 'a3a83df436d69821ea710f58f0846ea99a04df68320fb7fc0f3dd82d55c96fa92838eaf51fe8476ef038be962c3ef330312a0935a34396d1ab38a4dd87e0efe5'
